﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class GameController : MonoBehaviour {
	
	public static GameController Instance;
	public bool isPlayerTurn;
	public bool areEnemiesMoving;
	public int playerCurrentHealth = 50;
	public AudioClip gameOverSound;
	public int deathType = 0;
	public bool pauseGame;
	
	private BoardController boardController;
	private List<Enemy> enemies;
	private GameObject startImage;
	private Text startText;
	private GameObject levelImage;
	private Text levelText;
	private bool settingUpGame;
	private int secondsUntilLevelStart = 2;
	private int currentLevel = 0;
<<<<<<< HEAD

	

	public void StartLevel ()	
	{
			Application.LoadLevel (1);
		}

	public void StartLevel2(){
		Application.LoadLevel (2);
	}
	public void StartLevel3(){
		Application.LoadLevel (5);
	}
	public void StartLevel4(){
		Application.LoadLevel (6);
	}
	public void StartLevel5(){
		Application.LoadLevel (7);
	}
	public void StartLevel6(){
		Application.LoadLevel (8);
	}
	public void goToSelectScreen(){
		Application.LoadLevel (3);
	}

=======
	
	
	
	public void StartLevel ()	
	{
		Application.LoadLevel (1);
	}
	
>>>>>>> 4107aaa971894e60fef07c77da6a5de2dab8c5ce
	
	// Use this for initialization
	void Awake () {
		
		if (Instance != null && Instance != this) {
			DestroyImmediate (gameObject);
			return;
		}
		
		Instance = this;
		DontDestroyOnLoad (gameObject);
		boardController = GetComponent<BoardController> ();
		enemies = new List<Enemy> ();
	}
	void Start()
	{
		StartGame ();
<<<<<<< HEAD

	}

=======
		
	}
	
>>>>>>> 4107aaa971894e60fef07c77da6a5de2dab8c5ce
	private void StartGame(){
		settingUpGame = true;
		startImage = GameObject.Find ("Start Game");
		startText = GameObject.Find ("Start Text").GetComponent<Text> ();
		enemies.Clear ();
<<<<<<< HEAD
		//SoundController.Instance.music.Stop ();
	}

	private void DisableStartImage(){
		pauseGame = false;
=======
		startImage.SetActive (true);
	}
	
	private void DisableStartImage(){
>>>>>>> 4107aaa971894e60fef07c77da6a5de2dab8c5ce
		startImage.SetActive (false);
		settingUpGame = false;
		isPlayerTurn = true;
		areEnemiesMoving = false;
		InitializeGame ();
	}
	
	private void InitializeGame()
	{
<<<<<<< HEAD

=======
		
>>>>>>> 4107aaa971894e60fef07c77da6a5de2dab8c5ce
		if (currentLevel > 0) {
			startImage = GameObject.Find ("Start Image");
			startImage.SetActive (false);
		}
		pauseGame = true;
		settingUpGame = true;
		levelImage = GameObject.Find ("Level Image");
		levelText = GameObject.Find ("Level Text").GetComponent<Text>();
		levelText.text = "Day " + currentLevel;
		levelImage.SetActive (true);
		enemies.Clear ();
		boardController.SetupLevel (currentLevel);
		Invoke ("DisableLevelImage", secondsUntilLevelStart);
<<<<<<< HEAD
=======
		
>>>>>>> 4107aaa971894e60fef07c77da6a5de2dab8c5ce
	}
	
	private void DisableLevelImage()
	{
		pauseGame = false;
		levelImage.SetActive (false);
		//settingUpGame = false;
		isPlayerTurn = true;
		areEnemiesMoving = false;
	}
	
	private void OnLevelWasLoaded(int levelLoaded)
	{
		currentLevel++;
		InitializeGame ();
	}
	
	// Update is called once per frame
	void Update () {
		if (Input.GetKey ("escape"))
			Application.Quit ();
<<<<<<< HEAD

=======
		
>>>>>>> 4107aaa971894e60fef07c77da6a5de2dab8c5ce
		if (isPlayerTurn || areEnemiesMoving || pauseGame) {
			return;
		}
		
		StartCoroutine (MoveEnemies ());
	}
	
	private IEnumerator MoveEnemies()
	{
		areEnemiesMoving = true;
		
		yield return new WaitForSeconds (0.2f);
		
		foreach (Enemy enemy in enemies) {
			enemy.MoveEnemy ();
			yield return new WaitForSeconds(enemy.moveTime);
		}
		
		areEnemiesMoving = false;
		isPlayerTurn = true;
	}
	
	public void AddEnemyToList(Enemy enemy)
	{
		enemies.Add (enemy);
	}
	
	public void GameOver()
	{
		pauseGame = true;
		isPlayerTurn = false;
		SoundController.Instance.music.Stop ();
		SoundController.Instance.PlaySingle (gameOverSound);
		string deathOccurred = "starved";
<<<<<<< HEAD
		SoundController.Instance.music.Stop ();
=======
>>>>>>> 4107aaa971894e60fef07c77da6a5de2dab8c5ce
		
		// Determine cause of death
		switch(deathType)
		{
		case 1:
			deathOccurred = "were killed by an enemy";
			break;
		}
		
<<<<<<< HEAD
		levelText.text = "GAME OVER\n\n\n\n\n" + "You " + deathOccurred + " after " + currentLevel + " days...";
		
		levelImage.SetActive (true);
}
=======
		levelText.text = "You " + deathOccurred + " after " + currentLevel + " days...";
		
		levelImage.SetActive (true);
	}
>>>>>>> 4107aaa971894e60fef07c77da6a5de2dab8c5ce
}