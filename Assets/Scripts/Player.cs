﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Player : MovingObject {
	
	public Text healthText;
	public AudioClip movementSound1;
	public AudioClip movementSound2;
	public AudioClip chopSound1;
	public AudioClip chopSound2;
	public AudioClip foodSound1;
	public AudioClip heartSound1;
	
	private Animator animator;
	private int playerHealth;
	private int attackPower = 1;
	private int healthPerFruit = 5;
	private int healthPerGrape = 7;
	private int healthPerSoda = 10;
	private int healthPerTree = 9;
	private int secondsUntilNextLevel = 1;
	
	//You do not have access to local variables
	private int minScreenWidth;
	private int maxScreenWidth;
	private int tileSize;
	private int xAxis = 0;
	private int yAxis = 0;
	
	protected override void Start()
	{
		base.Start ();
		animator = GetComponent<Animator> ();
		playerHealth = GameController.Instance.playerCurrentHealth;
		healthText.text = "Health: " + playerHealth;
	}
	
	private void OnDisable()
	{
		GameController.Instance.playerCurrentHealth = playerHealth;
	}
	
	void Update () {
		if (!GameController.Instance.isPlayerTurn) {
			return;
		}
		
		CheckIfGameOver ();
<<<<<<< HEAD

		if (!GameController.Instance.pauseGame && CheckIfGameOver())
			GameController.Instance.GameOver();

=======
		
		if (!GameController.Instance.pauseGame && CheckIfGameOver())
			GameController.Instance.GameOver();
		
>>>>>>> 4107aaa971894e60fef07c77da6a5de2dab8c5ce
		if (Input.touchCount > 0) {
			Vector2 touchPosition = Input.GetTouch (0).position;
			xAxis = (int)touchPosition.x;
			yAxis = (int)touchPosition.y;
			
			GetScreenSize();
			
			if(xAxis >= minScreenWidth && xAxis <= maxScreenWidth){
				ExecuteTouch();
			}else{
				xAxis = 0;
				yAxis = 0;
			}
			
		} else {
			
			xAxis = (int)Input.GetAxisRaw ("Horizontal");
			yAxis = (int)Input.GetAxisRaw ("Vertical");
			
			if (xAxis != 0) {
				yAxis = 0;
			}
		}
		
		if (xAxis != 0 || yAxis != 0)
		{
			playerHealth--;
			healthText.text = "Health : " + playerHealth;
			SoundController.Instance.PlaySingle(movementSound1, movementSound2);
			//This moves the player and cchecks for wall collisions
			Move<Wall>(xAxis, yAxis);
			GameController.Instance.isPlayerTurn = false;
		}
	}
	
	private void GetScreenSize(){
		int screenWidth = Screen.currentResolution.width;
		int screenHeight = Screen.currentResolution.height;
		minScreenWidth = (screenWidth - screenHeight) / 2;
		maxScreenWidth = minScreenWidth + screenHeight;
		tileSize = screenHeight / 10;
	}
	
	private void ExecuteTouch(){
		Vector2 currentPosition = this.getPosition ();
		
		CorrectForScreens ();
		
		if (Mathf.Abs (xAxis - (int)currentPosition.x) > 
		    Mathf.Abs (yAxis - (int)currentPosition.y)) {
			MoveHorizontal (currentPosition);
		} else {
			MoveVertical (currentPosition);
		}
	}
	
	private void CorrectForScreens(){
		xAxis -= minScreenWidth;
		xAxis /= tileSize;
		yAxis /= tileSize;
	}
	
	//If our value is greater than zero when we do the subtraction we are going to move to the right
	//If it's the opposite then we are going to move to the left
	//setting it equal to 1 would mean moving to the right and setting it equal to -1 would mean moving to the left
	//If it's neither then we are moving nowhere
	private void MoveHorizontal(Vector2 currentPosition){
		yAxis = 0;
		if (xAxis - (int)currentPosition.x > 0) {
			xAxis = 1;
		}else if(xAxis - (int)currentPosition.x < 0){
			xAxis = -1;
		}else{
			xAxis = 0;
		}
	}
	
	private void MoveVertical(Vector2 currentPosition){
		xAxis = 0;
		if (yAxis - (int)currentPosition.y > 0) {
			yAxis = 1;
		}else if(yAxis - (int)currentPosition.y < 0){
			yAxis = -1;
		}else{
			yAxis = 0;
		}
	}
	
	private void OnTriggerEnter2D(Collider2D objectPlayerCollidedWith)
	{
		if (objectPlayerCollidedWith.tag == "Exit") 
		{
			Invoke ("LoadNewLevel", secondsUntilNextLevel);
			enabled = false;
			SoundController.Instance.PlaySingle(heartSound1);
			
		}
		else if(objectPlayerCollidedWith.tag == "Fruit")
		{
			playerHealth += healthPerFruit;
			healthText.text = "+" + healthPerFruit + " Health\n" + "Health: " + playerHealth;
			objectPlayerCollidedWith.gameObject.SetActive(false);
			SoundController.Instance.PlaySingle(foodSound1);
		}
		else if(objectPlayerCollidedWith.tag == "Soda")
		{
			playerHealth += healthPerSoda;
			healthText.text = "+" + healthPerSoda + " Health\n" + "Health: " + playerHealth;
			objectPlayerCollidedWith.gameObject.SetActive(false);
			SoundController.Instance.PlaySingle(foodSound1);
		}
		
		else if(objectPlayerCollidedWith.tag == "Grape")
		{
			playerHealth += healthPerGrape;
			healthText.text = "+" + healthPerGrape + " Health\n" + "Health: " + playerHealth;
			objectPlayerCollidedWith.gameObject.SetActive(false);
			SoundController.Instance.PlaySingle(foodSound1);
		}
		
		else if(objectPlayerCollidedWith.tag == "Tree")
		{
			playerHealth += healthPerTree;
			healthText.text = "+" + healthPerTree + " Health\n" + "Health: " + playerHealth;
			objectPlayerCollidedWith.gameObject.SetActive(false);
			SoundController.Instance.PlaySingle(foodSound1);
		}
	}
	
	private void LoadNewLevel()
	{
		Application.LoadLevel (Application.loadedLevel);
		if (!GameController.Instance.pauseGame)
		{
			GameController.Instance.pauseGame = true;
		}
	}
	
	protected override void HandleCollision<T>(T component)
	{
		Wall wall = component as Wall;
		animator.SetTrigger ("playerAttack");
		SoundController.Instance.PlaySingle (chopSound1, chopSound2);
		wall.DamageWall (attackPower);
	}
	
	public void TakeDamage(int damageReceived)
	{
		playerHealth -= damageReceived;
		healthText.text = "-" + damageReceived + " Health\n" + "Health: " + playerHealth;
		animator.SetTrigger ("playerHurt");
<<<<<<< HEAD

=======
		
>>>>>>> 4107aaa971894e60fef07c77da6a5de2dab8c5ce
		if (CheckIfGameOver())
			GameController.Instance.deathType = 1; // The player died by an enemy
	}
	
	private bool CheckIfGameOver()
	{
		if (playerHealth <= 0) 
		{
			GameController.Instance.GameOver();
		}
		return(playerHealth <= 0);
	}
	
}