﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class BoardController : MonoBehaviour {

	public int columns;
	public int rows;

	public GameObject[] floors; //This is our floor prefab we store it here so we can test if our script is going to work, this floors array selects a random floor tile
	public GameObject[] outerWalls; 
	public GameObject[] wallObstacles;
	public GameObject[] foodItems;
	public GameObject[] enemies;
	public GameObject exit;

	private Transform gameBoard;  //This is where all of our floor tiles are going to be created and stored
	private List<Vector3> obstaclesGrid;
	
		void Awake () {
		//we are initializing the obstaclesGrid
		obstaclesGrid = new List<Vector3>();
	}
	
 	void Update () {
	 
	}

	//We are trying to identify what the positions are and where can we put obstacles
	private void initializeObstaclePositions(){

		obstaclesGrid.Clear ();

		for (int x = 2; x < columns - 2; x++) {
			for(int y = 2; y < rows - 2; y++){
				//This stores our position and add it to our obstacles grid
				obstaclesGrid.Add(new Vector3(x, y, 0f));
			}
		}
	}

	//We are going to take our original floor tile and we are going to use that to place it on the correct position in our game board
	private void SetupGameBoard(){
		gameBoard = new GameObject ("Game Board").transform;

		//Use the loop in order to access each individual position on the game board
		//The 2 for loops are going to allow us to access each individual position of our game board
		for (int x = 0; x < columns; x++) {  //This is how we access each individual column
			for(int y = 0; y < rows; y++) //We are going to stop when we reach the end of the rows
			{

				GameObject selectedTile;
				//This determines whether or not we want to put an outter wall
				//x == 0 means we are in the first column, y == 0 means we are in the first row
				//x == columns - 1 means we are on the last column, y == 0 rows - 1 means we are in the last row
				//This if statement is making a choice to either make an outter wall or a floor tile
				if(x == 0 || y == 0 || x == columns - 1 || y == rows - 1)
				{
					//This is enables us to select the proper columns and rows in order to select the tile
					selectedTile = outerWalls[Random.Range(0, outerWalls.Length)];
				}
				else {
					//If we are not in the first or last one, we want to do the last part here
					selectedTile = floors[Random.Range (0, floors.Length)];
				}
				//Initialize a floor tile and put it on the game board and create a game object to a specific position
				//Use instantiate is to instantiate from a prefab and provide it in a position and we need to apply the z axis
				//Since this is a 2D game we set the z-axis to 0f, which means we're not concerned too mush with rotation
				GameObject floorTile = (GameObject)Instantiate(selectedTile, new Vector3(x, y, 0f), Quaternion.identity);
				//This is how we attach our floor tile to our game board
				//We are going to do this loop over and ove again to ensure that each individual floor tile gets attached to the correct position on the game board
				floorTile.transform.SetParent(gameBoard);
			}
		}
     }

	private void setRandomObstaclesOnGrid(GameObject[] obstaclesArray, int minimum, int maximum)
	{
		int obstacleCount = Random.Range (minimum, maximum + 1);

		if (obstacleCount > obstaclesGrid.Count) {
			obstacleCount = obstaclesGrid.Count;
		}
		for (int index = 0; index < obstacleCount; index++) {
			//The Random class is a class for generating random data
			GameObject selectedObstacle = obstaclesArray[Random.Range(0, obstaclesArray.Length)];
			Instantiate (selectedObstacle, SelectGridPosition(), Quaternion.identity);
		}
	}

	private Vector3 SelectGridPosition(){
		int randomIndex = Random.Range (0, obstaclesGrid.Count);
		Vector3 randomPosition = obstaclesGrid [randomIndex];
		obstaclesGrid.RemoveAt (randomIndex);
		return randomPosition;
	}

<<<<<<< HEAD
=======

>>>>>>> 4107aaa971894e60fef07c77da6a5de2dab8c5ce
	public void SetupLevel(int currentLevel)
	{
		initializeObstaclePositions ();
		//This will call our function
 		SetupGameBoard ();
		setRandomObstaclesOnGrid (wallObstacles, 3, 9);
		setRandomObstaclesOnGrid (foodItems, 1, 5);
		int enemyCount = (int)Mathf.Log (currentLevel, 2);
		setRandomObstaclesOnGrid (enemies, enemyCount, enemyCount);
		Instantiate (exit, new Vector3 (columns - 2, rows - 2, 0f), Quaternion.identity);
	}
}
